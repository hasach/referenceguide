DevOps

SDLC - Software Development Lifecycle

- Req. Analysis
- Design
- Implementation
- Testing
- Deployment/Maintenance/Support

Waterfall (Sequential Model)
Agile (Iterative and Incremental Model)

DevOps is a methodology to make IT high performing organisation by breaking down the wall of confusion between Dev and Ops together with structured framework.

- Collection of tools like Git, Jenkins, Sonar, Docker, Ansible, and many more
- Collection of tools and processes to deliver quality services
- A movement to respond faster to the market
- A movement to deliver quality services faster with the right mix of collaboration between Development and Operations Team


DevOps is a culture for Integration, Collaboration and Communication between different cross functional teams for Continuous Delivery.

DevOps encourages Operations to participate early in the Development Cycle so that products are designed to be easily deployable and maintainable.

DevOps emphasises on keeping WIP/inventory low and go to production as early as possible.

DevOps is not just a framework or a workflow. It's a culture that is overtaking the business world. DevOps ensures collaboration and communication between software engineers(Dev) and IT Operations(Ops). With DevOps, changes make it to production faster.


Establish DevOps by

- Continuous Integration
- Automated Testing
- Continuous Delivery
- Continuous Deployment
- Infrastructure as a Code
- Metrics (Reporting)
- Collaboration
- Making smart use of smart people

DevOps Skillset

- Project Management Tools
- Version Control Systems
- Build Tools
- Artifact Repository
- Code Quality, Code Coverage Tools
- CI/CD
- Automation and IaC
- Monitoring
